package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    //Get the student's entry year, extract first 2 digits of NPM
    public static long getEntryYear(long npm) {
        long result = npm / (long)Math.pow(10,12);
        return 2000 + result ;
    }

    //Method to get the jurusan from the next 2 digits of NPM
    public static String getJurusan(long npm){
        long result = (npm / (long)Math.pow(10,10)) % 100;

        //Get the jurusan of the student, based on the result of the NPM, using the given cases
        String jurusan;
        switch ((int) result){
            case 1 :
                jurusan = "Ilmu Komputer";
                break;
            case 2 :
                jurusan = "Sistem Informasi";
                break;
            case 3 :
                jurusan = "Teknologi Informasi";
                break;
            case 11 :
                jurusan = "Teknik Telekomunikasi";
                break;
            case 12 :
                jurusan = "Teknik Elektro";
                break;
            default :
                jurusan = "Invalid";
                break;
        }
        return jurusan;
    }

    //Method to get the date of birth from the NPM
    public static int[] getDateOfBirth(long npm) {
        //Array to store [dd,mm,yyyy]
        int[] dateOfBirth = new int[3];

        //Extract the needed integer characters
        long result = (npm / 100) % (long) Math.pow(10,8);

        //Operation to find the dd,mm,yyyy
        int yyyy = (int) (result % 10000);
        int mm = (int) (result /10000) % 10;
        int dd = (int) (result / Math.pow(10,6)) % 100;

        //Fill the array
        dateOfBirth[0] = dd;
        dateOfBirth[1] = mm;
        dateOfBirth[2] = yyyy;

        return dateOfBirth;

    }

    //Method to add all digits in the number to one digit
    public static int addDigitsTillOne(int digit){

        //Instantiate variable for result and digit
        int accum = digit;
        int result = 0;

        //Add the digits into the result
        while (accum != 0){
            int modulo = accum % 10;
            result += modulo;
            accum /= 10;
        }

        //Base case for recursion, if result < 10 will stop
        if (result < 10){
            return result;
        }
        //If result >= 10, will recurse to the function back again
        else {
            return addDigitsTillOne(result);
        }

    }

    //Method to get E
    public static int getE (long npm) {

        //Instantiate the necessary variables
        long npmVar = npm / 10;
        int accum = 0;
        int count = 0;

        //Perform the operation to get the E, by multiplying individual elements and add them
        while (npmVar >= 10){
            int firstDigit = (int) (npmVar / Math.pow(10,12-count));
            int lastDigit = (int) (npmVar % 10);

            accum += firstDigit * lastDigit;

            npmVar = (npmVar % (long) Math.pow(10,12-count)) / 10;
            count += 2;
        }

        accum += (int) npmVar;

        //Return the total sum of digits
        return addDigitsTillOne(accum);
    }


    public static boolean validate(long npm) {
        boolean isCorrectLength = (Math.pow(10,13) <= npm) && (npm < Math.pow(10,14));      //Has the correct length
        boolean inCorrectProdi = !(getJurusan(npm).equals("Invalid"));                      //In the major found in the campus
        boolean isNewInUni = (getEntryYear(npm) >= 2010);                                   //Enters uni after 2010
        boolean isOfAge = 2021 - getDateOfBirth(npm)[2] >= 15;                              //Is over 15 years old
        boolean hasCorrectE = getE(npm) == npm % 10;                                        //E is defined correctly

        return isCorrectLength && inCorrectProdi && isNewInUni && isOfAge && hasCorrectE;
    }

    //Returns string with given specs
    public static String extract(long npm) {
        String result = "";
        result += String.format("Tahun masuk: %d\n", getEntryYear(npm));
        result += String.format("Jurusan: %s\n",getJurusan(npm));
        result += String.format("Tanggal Lahir: %02d-%02d-%d",getDateOfBirth(npm)[0],getDateOfBirth(npm)[1],getDateOfBirth(npm)[2]);

        return result;
    }

    public static void main(String args[]) {

        //Asks input from user
        Scanner input = new Scanner(System.in);

        boolean exitFlag = false;
        while (!exitFlag) {

            //Receives input from user
            long npm = input.nextLong();

            //If npm is negative, breaks
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            else {
                if (validate(npm)) {
                    System.out.println(extract(npm));           //Return information of NPM if valid
                }
                else {
                    System.out.println("NPM tidak valid!");     //Return invalid NPM if invalid
                }
            }
        }

        //Close scanner
        input.close();
    }
}