package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    private int studentCount = 0;

    //Dummy Mahasiswa object to access the methods
    private static Mahasiswa methodAccessor = new Mahasiswa(null,0);

    public MataKuliah(String nama, int kapasitas) {
        //Constructor for MataKuliah
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    void addMahasiswa(Mahasiswa mahasiswa) {
        //Add Mahasiswa to selected course
        methodAccessor.append(mahasiswa,daftarMahasiswa,studentCount);
        studentCount++;
    }

    void dropMahasiswa(Mahasiswa mahasiswa) {
        //Drops Mahasiswa from selected course
        methodAccessor.remove(mahasiswa,daftarMahasiswa);
        studentCount--;
    }

    void addDosen(Dosen dosen) {
        //Add dosen
        this.dosen = dosen;
    }

    void dropDosen() {
        //Nullify dosen object
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public boolean equals(MataKuliah m) {
        return this.nama.equals(m.nama);
    }

    public int getCapacity(){
        return this.kapasitas;
    }

    public boolean isFull() {
        return studentCount == kapasitas;
    }

    public Dosen getDosen(){
        return this.dosen;
    }

    public int getStudentCount() {
        return this.studentCount;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
}