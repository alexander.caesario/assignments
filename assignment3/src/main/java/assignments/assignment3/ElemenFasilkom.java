package assignments.assignment3;

abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    protected String tipe;
    
    protected String nama;

    protected int friendship = 0;

    protected ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    protected int menyapaCount = 0;


    public void menyapa(ElemenFasilkom elemenFasilkom) {
        //Function for menyapa

        if (inArray(elemenFasilkom,telahMenyapa)){
            //If elemenFasilkom object is already in the array, reject
            System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini",this,elemenFasilkom));
            return;
        }

        if (this.tipe.equals("Mahasiswa")) {
            //If a student says hi to one of his lesson's lecturers, apply the friendship rule
            Mahasiswa student = (Mahasiswa) this;
            student.menyapaDosenIncrement();
        }

        //Append the elemenFasilkom to respective telah menyapa arrays
        append(elemenFasilkom,telahMenyapa,menyapaCount);
        append(this,elemenFasilkom.telahMenyapa,elemenFasilkom.menyapaCount);

        System.out.println(String.format("%s menyapa dengan %s",this,elemenFasilkom));

        //Increment both menyapaCount
        menyapaCount++;
        elemenFasilkom.menyapaCount++;
    }

    public void resetMenyapa() {

        //Initilise a new array to reset Menyapa at the end of every day
        telahMenyapa = new ElemenFasilkom[100];
        menyapaCount = 0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {

        //Cast food name and seller object
        ElemenKantin seller = (ElemenKantin) penjual;
        Makanan food = new Makanan(namaMakanan);

        if (!(seller.sells(food))){
            //Rejects if the seller doesn't sell selected food
            System.out.println(String.format("[DITOLAK] %s tidak menjual %s",seller,namaMakanan));
            return;
        }

        //Get the food object and apply friendship increment rules
        food = seller.getFood(food);
        System.out.println(String.format("%s berhasil membeli %s dengan harga %s",pembeli,namaMakanan,food.getHarga()));
        pembeli.incFriendship(1);
        penjual.incFriendship(1);
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public String getTipe() {
        return this.tipe;
    }

    public int getMenyapaCount() {
        return this.menyapaCount;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public void setFriendship(int friendship) {
        this.friendship = friendship;
    }

    public void append(Object o,Object[] arr,int i) {
        //int i = 0;

        //while (arr[i] != null){
            //i++;
        //}

        //Appends object to an index given by user
        arr[i] = o;
    }

    public void remove(Object o,Object[] arr) {

        /* Simply pop last element if it equals the last element
        to allow constant time best case */
        int n = arr.length - 1;
        if (arr[n] != null && arr[n].equals(o)) {
            arr[n] = null;
            return;
        }

        // Iterate to find object
        int i = index(o,arr);
        if (i == -1) return;

        //Iterate to shift and fill the replaced ones
        for (int j = i; j < n; j++){
            arr[j] = arr[j+1];
        }

        //Nullify last element
        arr[n] = null;
    }

    public int index(Object o,Object[] arr) {

        //Perform binary search to locate object
        for (int i = 0; arr[i] != null && i < arr.length; i++) {
            if (arr[i].toString().equals(o.toString())) {
                return i;
            }
        }

        //If not found return -1
        return -1;
    }

    public boolean inArray(Object o,Object[] arr) {
        //Check if object in array
        return index(o,arr) != -1;
    }

    public void incFriendship(int level) {
        //Increment friendship level
        this.friendship += level;
    }

    public boolean equals(ElemenFasilkom elem) {
        //Compare name to see if they are the same (Every element is guaranteed to have unique name)
        return this.nama.equals(elem.nama);
    }

    public int compareTo(ElemenFasilkom elem) {
        //Compare to ignore case the names
        return this.nama.compareToIgnoreCase(elem.nama);
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        //Returns telahMenyapa array
        return telahMenyapa;
    }
}