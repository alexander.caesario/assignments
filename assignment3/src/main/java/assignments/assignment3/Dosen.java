package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        //Constructor for Dosen object
        this.nama = nama;
        this.tipe = "Dosen";
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null) {
            //If Dosen has taught a course, it will reject
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s",this.nama,mataKuliah));
            return;
        }
        else if (mataKuliah.getDosen() != null) {
            //If the course already has a lecturer, it will reject
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar",mataKuliah));
            return;
        }

        //Add the new course attribute to Dosen object
        this.mataKuliah = mataKuliah;
        System.out.println(String.format("%s mengajar mata kuliah %s",this.nama,mataKuliah));
    }

    public void dropMataKuliah() {
        if (this.mataKuliah == null) {
            //If the lecturer is not teaching anything, it will reject
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun",this.nama));
            return;
        }

        //Drops the lecturer from the course, and set the course attribute of the lecturer to null
        System.out.println(String.format("%s berhenti mengajar %s",this.nama,this.mataKuliah));
        this.mataKuliah.dropDosen();
        this.mataKuliah = null;
    }

    public MataKuliah getMatkul() {
        return this.mataKuliah;
    }
}