package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {


    private Makanan[] daftarMakanan = new Makanan[10];
    private int foodCount = 0;

    public ElemenKantin(String nama) {
        //Constructor for ElemenKantin object
        this.nama = nama;
        this.tipe = "ElemenKantin";
    }

    public void setMakanan(String nama, long harga) {

        if (inArray(nama,daftarMakanan)){
            //If the food is in array, it will reject (each food has guaranteed unique name)
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar",nama));
            return;
        }

        //Append the food into array of foods sold by the seller
        Makanan food = new Makanan(nama,harga);
        append(food,daftarMakanan,foodCount);
        foodCount++;
        System.out.println(String.format("%s telah mendaftar makanan %s dengan harga makanan %d",this,food,food.getHarga()));
    }

    public boolean sells(Makanan food) {
        return inArray(food,daftarMakanan);
    }

    public Makanan getFood(Makanan food) {

        //Get food given from string/food, will return food from taken from the array
        int i = index(food,daftarMakanan);
        if (i != -1) {
            return daftarMakanan[i];
        }
        return null;
    }

    public Makanan[] getDaftarMakanan(){
        return this.getDaftarMakanan();
    }

}