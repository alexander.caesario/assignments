package assignments.assignment3;

class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;

    private long harga;

    public Makanan(String nama, long harga) {
        //Constructor for Makanan object
        this.nama = nama;
        this.harga = harga;
    }

    public Makanan(String nama) {
        //Constructor for Makanan object without harga
        this(nama,0);
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public boolean equals(Makanan m) {
        return this.nama.equals(m.nama);
    }

    public long getHarga() {
        return this.harga;
    }
}