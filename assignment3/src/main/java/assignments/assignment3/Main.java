package assignments.assignment3;

import java.util.Scanner;

public class Main {

    private ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    //Dummy Mahasiswa object to access the methods in Mahasiswa
    private static Mahasiswa methodAccessor = new Mahasiswa(null,0);

    private int totalMataKuliah = 0;

    private int totalElemenFasilkom = 0;

    private void addMahasiswa(String nama, long npm) {
        //Add new mahasiswa object to elemenFasilkom array
        Mahasiswa student = new Mahasiswa(nama,npm);
        methodAccessor.append(student,daftarElemenFasilkom,totalElemenFasilkom);
        totalElemenFasilkom++;
        System.out.println(String.format("%s berhasil ditambahkan",nama));
    }

    private void addDosen(String nama) {
        //Add new dosen object to elemenFasilkom array
        Dosen dosen = new Dosen(nama);
        methodAccessor.append(dosen,daftarElemenFasilkom,totalElemenFasilkom);
        totalElemenFasilkom++;
        System.out.println(String.format("%s berhasil ditambahkan",nama));
    }

    private void addElemenKantin(String nama) {
        //Add new elemenKantin to elemenFasilkom array
        ElemenKantin canteen = new ElemenKantin(nama);
        methodAccessor.append(canteen,daftarElemenFasilkom,totalElemenFasilkom);
        totalElemenFasilkom++;
        System.out.println(String.format("%s berhasil ditambahkan",nama));
    }

    private void menyapa(String objek1, String objek2) {

        //Gets element for each object
        ElemenFasilkom element1 = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek1);
        ElemenFasilkom element2 = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek2);

        if (element1.equals(element2)) {
            //If they are equal, then reject
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        }

        element1.menyapa(element2);
    }

    private void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);

        if (!(element.getTipe().equals("ElemenKantin"))){
            //If the element is not ElemenKantin, rejects
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin",objek));
            return;
        }

        ElemenKantin canteen = (ElemenKantin) element;
        canteen.setMakanan(namaMakanan,harga);
    }

    private void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom element2 = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek2);

        if(!(element2.getTipe().equals("ElemenKantin"))) {
            //If the second element is not ElemenKantin, rejects query
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin",element2));
            return;
        }

        ElemenKantin seller = (ElemenKantin) element2;
        ElemenFasilkom customer = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek1);

        if (seller.equals(customer)) {
            //If the seller buys food from itself, reject query
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            return;
        }

        customer.membeliMakanan(customer,seller,namaMakanan);

    }

    private void createMatkul(String nama, int kapasitas) {
        //Add matkul into the matkul array
        MataKuliah matkul = new MataKuliah(nama,kapasitas);
        methodAccessor.append(matkul,daftarMataKuliah,totalMataKuliah);
        totalMataKuliah++;
        System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %s",nama,kapasitas));
    }

    private void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);

        if (!(element.getTipe().equals("Mahasiswa"))) {
            //If the element is not a mahasiswa, reject query
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
            return;
        }

        //Add the given matkul into the student object
        MataKuliah matkul = (MataKuliah) getObject(daftarMataKuliah,namaMataKuliah);
        Mahasiswa student = (Mahasiswa) element;
        student.addMatkul(matkul);



    }

    private void dropMatkul(String objek, String namaMataKuliah) {

        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);
        if (!(element.getTipe().equals("Mahasiswa"))) {
            //If the element is not student, rejects query
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
            return;
        }

        //Remove a matkul from the student array
        MataKuliah matkul = (MataKuliah) getObject(daftarMataKuliah,namaMataKuliah);
        Mahasiswa student = (Mahasiswa) element;

        student.dropMatkul(matkul);
    }

    private void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);

        if (!(element.getTipe().equals("Dosen"))) {
            //If element is not a dosen, rejects query
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
            return;
        }

        Dosen dosen = (Dosen) element;
        MataKuliah matkul = (MataKuliah) getObject(daftarMataKuliah,namaMataKuliah);

        //Add dosen into lesson and vice versa
        dosen.mengajarMataKuliah(matkul);
        matkul.addDosen(dosen);

    }

    private void berhentiMengajar(String objek) {
        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);

        if (!(element.getTipe().equals("Dosen"))) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
            return;
        }

        //Drops dosen from lesson
        Dosen dosen = (Dosen) element;
        dosen.dropMataKuliah();

    }

    private void ringkasanMahasiswa(String objek) {
        ElemenFasilkom element = (ElemenFasilkom) getObject(daftarElemenFasilkom,objek);

        if (!(element.getTipe().equals("Mahasiswa"))) {
            //If element is not a student, reject query
            System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa",objek));
            return;
        }

        Mahasiswa student = (Mahasiswa) element;

        //Display the outline of the student
        System.out.println(String.format("Nama: %s",student.toString()));
        System.out.println(String.format("Tanggal Lahir: %s",student.getDOB()));
        System.out.println(String.format("Jurusan: %s",student.getJurusan()));
        System.out.println("Daftar Mata Kuliah: ");

        if (student.getMatkulCount() == 0) {
            System.out.println("Belum ada mata kuliah yang diambil");
            return;
        }

        for (int i = 0; i < student.getMatkulCount(); i++) {
            System.out.println(String.format("%d. %s",i+1,student.getDaftarMataKuliah()[i]));
        }

    }

    private void ringkasanMataKuliah(String namaMataKuliah) {

        //Display the outline of the course
        MataKuliah matkul = (MataKuliah) getObject(daftarMataKuliah,namaMataKuliah);

        System.out.println(String.format("Nama mata kuliah: %s",matkul));
        System.out.println(String.format("Jumlah mahasiswa: %s",matkul.getStudentCount()));
        System.out.println(String.format("Kapasitas: %s",matkul.getCapacity()));
        System.out.println(String.format("Dosen pengajar: %s",(matkul.getDosen() == null) ? "Belum ada" : matkul.getDosen()));
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        if (matkul.getStudentCount() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
            return;
        }

        for (int i = 0; i < matkul.getStudentCount(); i++) {
            System.out.println(String.format("%d. %s",i+1,matkul.getDaftarMahasiswa()[i]));
        }
    }

    private void nextDay() {
        //Recap the day, sort array and display them
        recapToday();
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate.");
        friendshipRanking();

        for (int i = 0; i < totalElemenFasilkom; i++) {
            System.out.println(String.format("%d. %s (%d)",i+1,daftarElemenFasilkom[i],daftarElemenFasilkom[i].getFriendship()));
        }
    }

    private void friendshipRanking() {
        //Selection sort algorithm, compare the friendship descendingly and then name lexicographically
        for (int i = 0; i < totalElemenFasilkom; i++) {
            int minInd = i;
            for (int j = i + 1; j < totalElemenFasilkom; j++) {
                if (daftarElemenFasilkom[j].getFriendship() > daftarElemenFasilkom[minInd].getFriendship()) {
                    minInd = j;
                }
                else if (daftarElemenFasilkom[j].getFriendship() == daftarElemenFasilkom[minInd].getFriendship()) {
                    int subResult = daftarElemenFasilkom[minInd].compareTo(daftarElemenFasilkom[j]);
                    if (subResult > 0) {
                        minInd = j;
                    }
                }

            }
            if (minInd != i) {
                ElemenFasilkom temp = daftarElemenFasilkom[i];
                daftarElemenFasilkom[i] = daftarElemenFasilkom[minInd];
                daftarElemenFasilkom[minInd] = temp;
            }
        }
    }

    private void programEnd() {
        //Prints out the elemenFasilkom array
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");

        for (int i = 0; i < totalElemenFasilkom; i++) {
            System.out.println(String.format("%d. %s (%d)",i+1,daftarElemenFasilkom[i],daftarElemenFasilkom[i].getFriendship()));
        }
    }

    private void recapToday() {

        //Apply the friendship rule
        for (int i = 0; i < totalElemenFasilkom; i++) {
            ElemenFasilkom element = daftarElemenFasilkom[i];

            if (element.getMenyapaCount() >= (totalElemenFasilkom - 1) / 2) {
                element.incFriendship(10);
            }
            else {
                element.incFriendship(-5);
            }

            if (element.getFriendship() < 0) {
                element.setFriendship(0);
            }

            if (element.getFriendship() > 100) {
                element.setFriendship(100);
            }

            //Reset each element's telahMenyapa array after applying friendship rule
            element.resetMenyapa();

        }
    }


    protected Object getObject(Object[] arr, String objek) {
        //Get object given index
        int i = methodAccessor.index(objek,arr);
        return arr[i];
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Main fasilkom = new Main();                 //Access the functions

        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                fasilkom.addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                fasilkom.addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                fasilkom.addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                fasilkom.menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                fasilkom.addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                fasilkom.membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                fasilkom.createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                fasilkom.addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                fasilkom.dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                fasilkom.mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                fasilkom.berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                fasilkom.ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                fasilkom.ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                fasilkom.nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                fasilkom.programEnd();
                break;
            }
        }
    }
}