package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];

    private int matkulCount;
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        //Constructor for mahasiswa
        this.nama = nama;
        this.npm = npm;
        this.tipe = "Mahasiswa";
        this.tanggalLahir = extractTanggalLahir(npm);
        this.jurusan = extractJurusan(npm);
    }

    public void addMatkul(MataKuliah mataKuliah) {
        if (inArray(mataKuliah,daftarMataKuliah)) {
            //If course is already in array, rejects query
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya",mataKuliah));
            return;
        }
        else if (mataKuliah.isFull()) {
            //If course is already full, rejects query
            System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya",mataKuliah));
            return;
        }


        //Append course object to list of courses in the student, and student object to list of student in courses
        append(mataKuliah,daftarMataKuliah,matkulCount);
        mataKuliah.addMahasiswa(this);
        matkulCount++;
        System.out.println(String.format("%s telah berhasil menambahkan mata kuliah %s",this,mataKuliah));
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        if (!(inArray(mataKuliah,daftarMataKuliah))) {
            //If course is never taken, rejects query
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil",mataKuliah));
            return;
        }

        //Remove course from the student's array and otherwise
        remove(mataKuliah,daftarMataKuliah);
        matkulCount--;
        mataKuliah.dropMahasiswa(this);
        System.out.println(String.format("%s berhasil drop mata kuliah %s",this,mataKuliah));
    }

    String extractTanggalLahir(long npm) {
        long result = (npm / 100) % (long) Math.pow(10,8);

        //Operation to find the dd,mm,yyyy
        int yyyy = (int) (result % 10000);
        int mm = (int) (result /10000) % 100;
        int dd = (int) (result / Math.pow(10,6)) % 100;

        return String.format("%d-%d-%d",dd,mm,yyyy);
    }

    String extractJurusan(long npm) {
        long result = (npm / (long)Math.pow(10,10)) % 100;

        /*return switch((int) result){
            case 1 -> "Ilmu Komputer";
            case 2 -> "Sistem Informasi";
            default -> null;
        };*/

        //Method to extract the jurusan
        String faculty;
        switch((int) result) {
            case 1: faculty = "Ilmu Komputer"; break;
            case 2: faculty = "Sistem Informasi"; break;
            default : faculty = null;
        }
        return faculty;
    }

    public void menyapaDosenIncrement() {

        for (int i = 0; i < menyapaCount; i++) {
            //Iterate through telahMenyapa array to find if he/she has say hi to dosen
            if (telahMenyapa[i].getTipe().equals("Dosen")) {
                for (int j = 0; j < matkulCount; j++) {
                    //Get the dosen for each matkul
                    Dosen dosen = daftarMataKuliah[j].getDosen();
                    if ((dosen != null) && (telahMenyapa[i].equals(dosen))){
                        //If he has said hi to one of his/her lesson's lecturers, apply friendship rule
                        incFriendship(2);
                        dosen.incFriendship(2);
                    }
                }
            }
        }
    }

    public long getNPM(){
        return this.npm;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public String getDOB() {
        return this.tanggalLahir;
    }

    public int getMatkulCount() {
        return this.matkulCount;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }
}